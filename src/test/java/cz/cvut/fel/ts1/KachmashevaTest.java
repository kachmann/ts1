package cz.cvut.fel.ts1;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class KachmashevaTest {

    @Test
    public void factorial_nIs5_returns120() {
        // Arrange
        Kachmasheva kachmasheva = new Kachmasheva();
        int n = 5;
        int expected = 120;

        // Act
        int actual = (int) kachmasheva.factorial(n);

        // Assert
        Assertions.assertEquals(expected,actual);
    }

    @Test
    public void factorial_nIsMinus_returns0() {
        // Arrange
        Kachmasheva kachmasheva = new Kachmasheva();
        int n = -1;
        int expected = 0;

        // Act
        int actual = (int) kachmasheva.factorial(n);

        // Assert
        Assertions.assertEquals(expected,actual);
    }

    @Test
    public void factorial_nIs0_returns1() {
        // Arrange
        Kachmasheva kachmasheva = new Kachmasheva();
        int n = 0;
        int expected = 1;

        // Act
        int actual = (int) kachmasheva.factorial(n);

        // Assert
        Assertions.assertEquals(expected,actual);
    }
}
